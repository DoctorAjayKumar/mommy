# Mommy

![](images/mommy-870.jpg)

This is a browser extension that listens to messages between an Aepp
and a Waellet, and records them.

It's currently coded against Firefox because that's what I use.
Eventually it will also work with Chrom(e|ium).

# Installing/Using

1.  Clone the repository

        git clone https://gitlab.com/DoctorAjayKumar/mommy.git

2.  In Firefox, open `about:debugging`
3.  Click `This Firefox`:

    ![](images/about_debugging.png)

4.  Click `Load Temporary Add-on...`

    ![](images/load-temp-addon.png)

5.  Select the `manifest.json` file for this project

    ![](images/mommy-manifest.png)

6.  It should be loaded now

    ![](images/mommy-loaded.png)
